<?php
  /* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 2.0.0-12                                               |
  | http://www.elastix.org                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
  $Id: paloSantoRecordingGroups.class.php,v 1.1 2010-01-27 02:01:42 Eduardo Cueva ecueva@palosanto.com Exp $ */
class paloSantoRecordingGroups {
    var $_DB;
    var $errMsg;

    function paloSantoRecordingGroups(&$pDB)
    {
        // Se recibe como parámetro una referencia a una conexión paloDB
        if (is_object($pDB)) {
            $this->_DB =& $pDB;
            $this->errMsg = $this->_DB->errMsg;
        } else {
            $dsn = (string)$pDB;
            $this->_DB = new paloDB($dsn);

            if (!$this->_DB->connStatus) {
                $this->errMsg = $this->_DB->errMsg;
                // debo llenar alguna variable de error
            } else {
                // debo llenar alguna variable de error
            }
        }
    }

    /*HERE YOUR FUNCTIONS*/
    function getGroups()
    {
    	$arrParam = null;
    	$query   = "SELECT group_title FROM groups ORDER BY group_title";
    	$result=$this->_DB->fetchTable($query, true, $arrParam);
    
    	if($result==FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return array();
    	}
    	 
    	$test = null;
    	foreach ($result as $value) {
    		$arrayGroups[$value["group_title"]] =  $value["group_title"];
    	}
    	 
    	return $arrayGroups;
    }
    
    function getAllUsers($users)
    {
    	$arrayUsers = Null;
    	 
    	foreach ($users as $value){
    		$arrayUsers[$value[1]] =$value[1];
    	}
    
    	return $arrayUsers;
    }
    
    function addToRecordingGroup($user,$group)
    {
    	$arrParam = array($group,$user);
    	$query   = "INSERT INTO recordings_group (group_title,group_user) VALUES (?,?)";
    	$result=$this->_DB->genQuery($query,$arrParam);
    
    	if($result == FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    }
    
    function newRecordingGroup($user)
    {
    	$arrParam = array($user);
    	$query   = "INSERT INTO groups (group_title) VALUES (?)";
    	$result=$this->_DB->genQuery($query,$arrParam);
    
    	if($result == FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    
    }

    function getNumRecordingGroups()
    {
        $query   = "SELECT COUNT(*) FROM groups";

        $result=$this->_DB->getFirstRowQuery($query);

        if($result==FALSE){
            $this->errMsg = $this->_DB->errMsg;
            return 0;
        }
        return $result[0];
    }

    function getRecordingGroups($limit, $offset)
    {
    	$arrParam = array($limit,$offset);
        $query   = "SELECT group_title FROM groups limit ? OFFSET ?";

        $result=$this->_DB->fetchTable($query, true, $arrParam);

        if($result==FALSE){
            $this->errMsg = $this->_DB->errMsg;
            return array();
        }
        return $result; 
    }
    
    function getRecordingUsers($group_title)
    {
    	$arrParam = array($group_title);
    	$query   = "SELECT group_user FROM recordings_group WHERE group_title = ? ORDER BY group_user";
    	$result=$this->_DB->fetchTable($query, true, $arrParam);
  
    	$usr = array();
    	foreach($result as $key => $value){
    		array_push($usr,$value['group_user']);
    	}
    	
    	return implode(", ",$usr);
    }
    
    function getGroupExtensions($group_title)
    {
    	$arrParam = array($group_title);
    	$query   = "SELECT g.extension  FROM group_extensions as g WHERE g.group_title = ? ORDER BY g.extension";
    	$result=$this->_DB->fetchTable($query, true, $arrParam);
    	 
    	$ext = array();
    	foreach($result as $key => $value){
    		array_push($ext,$value['extension']);
    	}
    	 
    	return implode(", ",$ext);
    }
    
    function getExtensionsById($id)
    {
    	$arrParam = array($id);
    	$query   = "SELECT g.extension  FROM group_extensions as g WHERE g.group_title = ? ORDER BY g.extension";
    	$result=$this->_DB->fetchTable($query, true, $arrParam);
    	
    	$ext = null;
    	foreach ($result as $value) {
    		$ext[$value["extension"]] =  $value["extension"];
    	}
    	return $ext;
    }
    
    function getUsersById($id)
    {
    	$arrParam = array($id);
    	$query   = "SELECT group_user  FROM recordings_group  WHERE group_title = ?";
    	$result=$this->_DB->fetchTable($query, true, $arrParam);
    	 
    	$usr = null;
    	foreach ($result as $value) {
    		$usr[$value["group_user"]] =  $value["group_user"];
    	}
    	return $usr;
    }
    
    function deleteGroup($id)
    {
    	$data = array($id);   	
    	$query = "DELETE FROM groups WHERE group_title = ?";
    	$result = $this->_DB->genQuery($query, $data);
    	
    	if($result==FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    }
    
    function getRecordingsExtensions()
    {
    	$query = "SELECT extension FROM users ORDER BY extension";
        $result=$this->_DB->fetchTable($query,true);
        
        $ext = null;
        foreach ($result as $value) {
        	$ext[$value["extension"]] =  $value["extension"];
        }

        if($result==FALSE){
            $this->errMsg = $this->_DB->errMsg;
            return null;
        }
        return $ext;
    }
    
    
    function addExtension($group,$ext)
    {
    	$arrParam = array($group,$ext);
    	$query   = "INSERT INTO group_extensions (group_title, extension) VALUES (?,?)";
    	$result=$this->_DB->genQuery($query,$arrParam);
    
    	if($result == FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    }
    
    function delExtension($id,$ext)
    {
    	$arrParam = array($id,$ext);
    	$query   = "DELETE FROM group_extensions WHERE group_title = ? AND extension = ?";
    	$result=$this->_DB->genQuery($query,$arrParam);
    
    	if($result == FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    }
    
    function delUserFromGroup($id,$ext)
    {
    	$arrParam = array($id,$ext);
    	$query   = "DELETE FROM recordings_group WHERE group_title = ? AND group_user = ?";
    	$result=$this->_DB->genQuery($query,$arrParam);
    
    	if($result == FALSE){
    		$this->errMsg = $this->_DB->errMsg;
    		return false;
    	}
    	return true;
    }
}
?>
