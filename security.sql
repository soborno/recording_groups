-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2017 a las 02:11:52
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `security`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `group_title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `group_extensions`
--

CREATE TABLE `group_extensions` (
  `group_title` varchar(100) NOT NULL,
  `extension` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recordings_group`
--

CREATE TABLE `recordings_group` (
  `group_title` varchar(100) NOT NULL,
  `group_user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_title`);

--
-- Indices de la tabla `group_extensions`
--
ALTER TABLE `group_extensions`
  ADD PRIMARY KEY (`group_title`,`extension`);

--
-- Indices de la tabla `recordings_group`
--
ALTER TABLE `recordings_group`
  ADD PRIMARY KEY (`group_title`,`group_user`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `group_extensions`
--
ALTER TABLE `group_extensions`
  ADD CONSTRAINT `fk_id` FOREIGN KEY (`group_title`) REFERENCES `groups` (`group_title`) ON DELETE CASCADE;

--
-- Filtros para la tabla `recordings_group`
--
ALTER TABLE `recordings_group`
  ADD CONSTRAINT `fk_group` FOREIGN KEY (`group_title`) REFERENCES `groups` (`group_title`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
