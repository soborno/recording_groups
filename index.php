<?php
  /* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4:
  Codificación: UTF-8
  +----------------------------------------------------------------------+
  | Elastix version 2.0.0-12                                               |
  | http://www.elastix.org                                               |
  +----------------------------------------------------------------------+
  | Copyright (c) 2006 Palosanto Solutions S. A.                         |
  +----------------------------------------------------------------------+
  | Cdla. Nueva Kennedy Calle E 222 y 9na. Este                          |
  | Telfs. 2283-268, 2294-440, 2284-356                                  |
  | Guayaquil - Ecuador                                                  |
  | http://www.palosanto.com                                             |
  +----------------------------------------------------------------------+
  | The contents of this file are subject to the General Public License  |
  | (GPL) Version 2 (the "License"); you may not use this file except in |
  | compliance with the License. You may obtain a copy of the License at |
  | http://www.opensource.org/licenses/gpl-license.php                   |
  |                                                                      |
  | Software distributed under the License is distributed on an "AS IS"  |
  | basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See  |
  | the License for the specific language governing rights and           |
  | limitations under the License.                                       |
  +----------------------------------------------------------------------+
  | The Original Code is: Elastix Open Source.                           |
  | The Initial Developer of the Original Code is PaloSanto Solutions    |
  +----------------------------------------------------------------------+
  $Id: index.php,v 1.1 2010-01-27 02:01:42 Eduardo Cueva ecueva@palosanto.com Exp $ */
//include elastix framework
include_once "libs/paloSantoGrid.class.php";
include_once "libs/paloSantoForm.class.php";
include_once "libs/misc.lib.php";

function _moduleContent(&$smarty, $module_name)
{
    //include module files
    include_once "modules/$module_name/configs/default.conf.php";
    include_once "modules/$module_name/libs/paloSantoRecordingGroups.class.php";


    //include file language agree to elastix configuration
    //if file language not exists, then include language by default (en)
    $lang=get_language();
    $base_dir=dirname($_SERVER['SCRIPT_FILENAME']);
    $lang_file="modules/$module_name/lang/$lang.lang";
    if (file_exists("$base_dir/$lang_file")) include_once "$lang_file";
    else include_once "modules/$module_name/lang/en.lang";

    //global variables
    global $arrConf;
    global $arrConfModule;
    global $arrLang;
    global $arrLangModule;
    $arrConf = array_merge($arrConf,$arrConfModule);
    $arrLang = array_merge($arrLang,$arrLangModule);

    //folder path for custom templates
    $templates_dir=(isset($arrConf['templates_dir']))?$arrConf['templates_dir']:'themes';
    $local_templates_dir="$base_dir/modules/$module_name/".$templates_dir.'/'.$arrConf['theme'];

    //conexion resource
    
    //Security
    $dsn = generarDSNSistema("root","security");
    $pDB = new paloDB($dsn);
    
    //Asterisk
    $pConfig = new paloConfig("/etc", "amportal.conf", "=", "[[:space:]]*=[[:space:]]*");
    $arrConfig = $pConfig->leer_configuracion(false);
    $dsn2 = $arrConfig['AMPDBENGINE']['valor'] . "://" . $arrConfig['AMPDBUSER']['valor'] . ":" . $arrConfig['AMPDBPASS']['valor'] . "@" . $arrConfig['AMPDBHOST']['valor'] . "/asterisk";
    $pDB2 = new paloDB($dsn2);
    
    //Elastix ACL
    $pDB3 = new paloDB($arrConf['elastix_dsn']['acl']);	 

    //actions
    $action = getAction(); 
    $content = "";
    switch($action){
    	
		case "new_rate":
			    $content = reportAssignGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "deluser":
                $content = reportDeleteFromGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
		case "import_rate":
				$content = reportNewGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "view_form":
			    $content = reportViewRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
				break;
        case "edit_form":
			    $content = reportRecordingGroupEdit($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
				break;
        case "save_new":
                $content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "delete":
                $content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "add_extension_form":
               	$content = reportAddExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "del_extension_form":
                $content = reportDelExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "save_import":
                $content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "save_edit":
                $content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "save_extension":
               	$content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;
        case "submit_delete_extension":
               	$content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
                break;           
        default:
            	$content = reportRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
            	break;
        case "submit_delete_from_group":
           		$content = obtainResultForm($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
            	break;
    }
    return $content;
}

/**
 * Vista por Defecto
 * @param unknown $smarty
 * @param unknown $module_name
 * @param unknown $local_templates_dir
 * @param unknown $pDB
 * @param unknown $pDB2
 * @param unknown $pDB3
 * @param unknown $arrConf
 * @param unknown $arrLang
 * @return unknown
 */
function reportRecordingGroup($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang)
{
    $pRecordingGroups = new paloSantoRecordingGroups($pDB);
    
    $action = getParameter("nav");
    $start  = getParameter("start");
    $as_csv = getParameter("exportcsv");
    $arrResult  = "";
    $arrColumns = "";
	    
	$action = getAction();

    //begin grid parameters
    $oGrid  = new paloSantoGrid($smarty);
    $totalRecordingGroups = $pRecordingGroups->getNumRecordingGroups();

    $url = array(
        'menu' =>  $module_name,
    );
 
    $oGrid->pagingShow(true); // show paging section.
    $oGrid->setTitle(_tr("Grupos de Escucha"));
    $oGrid->setIcon("modules/$module_name/images/reports_billing_rates.png");
    $oGrid->setNameFile_Export("Grupos de Escucha");
    $oGrid->setURL($url);
    $oGrid->addNew("import_rate",_tr("Nuevo Grupo"));
    //oGrid->customAction("import_rate",_tr("Nuevo Grupo"));
    $oGrid->customAction("new_rate",_tr("Agregar Usuario"));
    //$oGrid->customAction("delete_group",_tr("Quitar Grupo"));

    $smarty->assign("module_name", $module_name);

    $arrData = null; 
  
    //GRID con el GRUPO de Escuchas
    $limit  = 20;
    $oGrid->setLimit($limit);
    $oGrid->setTotal($totalRecordingGroups);
    $offset = $oGrid->calculateOffset();
    $arrResult = $pRecordingGroups->getRecordingGroups($limit, $offset);
    if(is_array($arrResult) && $totalRecordingGroups>0){
    	foreach($arrResult as $key => $value){
    		$arrTmp[0] = "";
			$arrTmp[1] = $value['group_title'];
			$arrTmp[2] = $pRecordingGroups->getRecordingUsers($value['group_title']);
			$arrTmp[3] = $pRecordingGroups->getGroupExtensions($value['group_title']);
			$arrTmp[4] = "<a href='?menu=$module_name&action=view&id=".$value['group_title']."'>"._tr("View")."</a>";
			$arrData[] = $arrTmp;
    	}
    }
    // arreglo de columnas
    $arrColumns  = array(_tr(""), _tr("Nombre"), _tr("Usuarios"), _tr("Extensiones"), _tr("View"));
    
    $oGrid->setColumns($arrColumns);
    $oGrid->setData($arrData);
    
    $content = $oGrid->fetchGrid();

    return $content;
}

/**
 * Formulario de Nuevo Grupo
 * @param unknown $smarty
 * @param unknown $module_name
 * @param unknown $local_templates_dir
 * @param unknown $pDB
 * @param unknown $pDB2
 * @param unknown $pDB3
 * @param unknown $arrConf
 * @param unknown $arrLang
 * @return string
 */
function reportNewGroup($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	$_DATA  = $_POST;

	$arrForm = createNewGroupForm($arrLang);
	$oForm = new paloForm($smarty,$arrForm);

	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("SAVE", $arrLang["SAVE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);

	$htmlForm = $oForm->fetchForm("$local_templates_dir/new_group.tpl",$arrLang["New_group"], $_DATA);
	$content  = "<form  method='POST' enctype='multipart/form-data' style='margin-bottom:0;' action='?menu=$module_name'>".$htmlForm."</form>";
	return $content;

}

/**
 * Formulario de Asociaci�n Grupo-Usuario
 * @param unknown $smarty
 * @param unknown $module_name
 * @param unknown $local_templates_dir
 * @param unknown $pDB
 * @param unknown $pDB2
 * @param unknown $pDB3
 * @param unknown $arrConf
 * @param unknown $arrLang
 * @return string
 */
function reportAssignGroup($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
    //begin, Form data persistence to errors and other events.
    $_DATA  = $_POST;
    
    //Datos BD Security
	$pClass = new paloSantoRecordingGroups($pDB);
	$groups = $pClass->getGroups();
	
	//Datos BD Elastix
	$pACL = new paloACL($pDB3);
	$pClassE = new paloSantoRecordingGroups($pACL);
	$usersEE = $pACL->getUsers();
	$usersE = $pClassE->getAllUsers($usersEE);
	
	//Comienza Formulario
	$arrForm = createFormNew($usersE,$groups);
    $oForm = new paloForm($smarty,$arrForm);
    
	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("SAVE", $arrLang["SAVE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	$smarty->assign("EDIT", $arrLang["EDIT"]);
    $smarty->assign("DELETE", $arrLang["DELETE"]);
	$smarty->assign("module_name", $module_name);

    $htmlForm = $oForm->fetchForm("$local_templates_dir/assign_group.tpl",$arrLang["Assign_group"], $_DATA);
    $content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name'>".$htmlForm."</form>";
    return $content;

}

function reportDeleteFromGroup($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	//begin, Form data persistence to errors and other events.
	$_DATA  = $_POST;
	$id = getParameter("id");

	//Datos BD Security
	$pClass = new paloSantoRecordingGroups($pDB);
	$ext = $pClass->getUsersById($id);

	//Comienza Formulario
	$arrForm = createDeleteGroup($ext);
	$oForm = new paloForm($smarty,$arrForm);

	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("REMOVE", $arrLang["REMOVE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	$smarty->assign("module_name", $module_name);

	$htmlForm = $oForm->fetchForm("$local_templates_dir/delete_from_group.tpl",$arrLang["DELETEUSER"], $_DATA);
	$content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name&id=$id'>".$htmlForm."</form>";
	return $content;

}

/**
 * Pantalla que muestra los usuarios y extensiones de un grupo determinado
 * @param unknown $smarty
 * @param unknown $module_name
 * @param unknown $local_templates_dir
 * @param unknown $pDB
 * @param unknown $pDB2
 * @param unknown $pDB3
 * @param unknown $arrConf
 * @param unknown $arrLang
 * @return string
 */
function reportViewRecordingGroup($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	//begin, Form data persistence to errors and other events.
	$_DATA  = $_POST;
	
	$id = getParameter("id");	
	$pRecordingGroups = new paloSantoRecordingGroups($pDB);	
	$users = $pRecordingGroups->getRecordingUsers($id);
	$extensions = $pRecordingGroups->getGroupExtensions($id);

	$arrForm = " ";
	$oForm = new paloForm($smarty,$arrForm);

	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("SAVE", $arrLang["SAVE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	$smarty->assign("DELETEUSER", $arrLang["DELETEUSER"]);
	$smarty->assign("ADDEXTENSION", $arrLang["ADDEXTENSION"]);
	$smarty->assign("DELEXTENSION", $arrLang["DELEXTENSION"]);
	$smarty->assign("DELETE", $arrLang["DELETE"]);
	$smarty->assign("module_name", $module_name);
	$smarty->assign("ID", $id);
	$smarty->assign("USERS", $users);
	$smarty->assign("EXTENSIONS", $extensions);
	$smarty->assign("CONFIRM_CONTINUE", $arrLang['CONFIRM_CONTINUE']);

	$htmlForm = $oForm->fetchForm("$local_templates_dir/view_group.tpl",$arrLang["Recording_group"], $_DATA);
	$content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name&id=$id'>".$htmlForm."</form>";
	return $content;

}

function reportRecordingGroupEdit($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	 //begin, Form data persistence to errors and other events.
     $_DATA  = $_POST;
     $id = getParameter("id");
     
	 $pClass = new paloSantoRecordingGroups($pDB);
	 $pClassAsterisk = new paloSantoRecordingGroups($pDB2);
	 
	 $extensions = $pClass->getGroupExtensions($id);
	 
	 $arrTrunks = $pClassAsterisk->getRecordingsExtensions();
	 
	 $arrForm = createEditForm($arrLang,$arrTrunks);
     $oForm = new paloForm($smarty,$arrForm);
     
	 $smarty->assign("CANCEL", $arrLang["CANCEL"]);
	 $smarty->assign("APPLY_CHANGES", $arrLang["APPLY_CHANGES"]);
	 $smarty->assign("DELETE", $arrLang["DELETE"]);
	 $smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	 $smarty->assign("EDIT", $arrLang["EDIT"]);
	 $smarty->assign("module_name", $module_name);
	 $smarty->assign("Current_Extension",_tr("Current Extensions"));
	 $smarty->assign("ID", $id);
	 $smarty->assign("EXTENSIONS", $extensions);

     $htmlForm = $oForm->fetchForm("$local_templates_dir/edit_group.tpl",$arrLang["Edit Group"], $_DATA);
     $content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name&id=$id&namerate=".$arrData['name']."'>".$htmlForm."</form>";
     return $content;
//
}

function reportAddExtension($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	//begin, Form data persistence to errors and other events.
	$_DATA  = $_POST;
	$id = getParameter("id");
	 
	$pClass = new paloSantoRecordingGroups($pDB);
	$pClassAsterisk = new paloSantoRecordingGroups($pDB2);

	$extensions = $pClass->getGroupExtensions($id);

	$arrExt = $pClassAsterisk->getRecordingsExtensions();
	//$arrTrunks = $pClass->getTrunks($pDB2);

	$arrForm = createAddExtension($arrExt);
	$oForm = new paloForm($smarty,$arrForm);
	 
	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("APPLY_CHANGES", $arrLang["APPLY_CHANGES"]);
	$smarty->assign("DELETE", $arrLang["DELETE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	$smarty->assign("EDIT", $arrLang["EDIT"]);
	$smarty->assign("module_name", $module_name);
	$smarty->assign("Current_Extension",_tr("Current Extensions"));
	$smarty->assign("ID", $id);
	$smarty->assign("EXTENSIONS", $extensions);

	$htmlForm = $oForm->fetchForm("$local_templates_dir/add_extension.tpl",$arrLang["ADDEXTENSION"], $_DATA);
	$content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name&id=$id&namerate=".$arrData['name']."'>".$htmlForm."</form>";
	return $content;
	//
}

function reportDelExtension($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
	//begin, Form data persistence to errors and other events.
	$_DATA  = $_POST;
	$id = getParameter("id");

	$pClass = new paloSantoRecordingGroups($pDB);
	//$pClassAsterisk = new paloSantoRecordingGroups($pDB2);

	$extensions = $pClass->getGroupExtensions($id);

	$arrExt = $pClass->getExtensionsById($id);

	$arrForm = createDelExtension($arrExt);
	$oForm = new paloForm($smarty,$arrForm);

	$smarty->assign("CANCEL", $arrLang["CANCEL"]);
	$smarty->assign("APPLY_CHANGES", $arrLang["APPLY_CHANGES"]);
	$smarty->assign("DELETE", $arrLang["DELETE"]);
	$smarty->assign("REQUIRED_FIELD", $arrLang["REQUIRED_FIELD"]);
	$smarty->assign("EDIT", $arrLang["EDIT"]);
	$smarty->assign("module_name", $module_name);
	$smarty->assign("Current_Extension",_tr("Current Extensions"));
	$smarty->assign("ID", $id);
	$smarty->assign("EXTENSIONS", $extensions);

	$htmlForm = $oForm->fetchForm("$local_templates_dir/del_extension.tpl",$arrLang["DELEXTENSION"], $_DATA);
	$content  = "<form  method='POST' style='margin-bottom:0;' action='?menu=$module_name&id=$id&namerate=".$arrData['name']."'>".$htmlForm."</form>";
	return $content;
	//
}

function obtainResultForm($smarty, $module_name, $local_templates_dir, &$pDB, &$pDB2, &$pDB3, $arrConf, $arrLang){
    $_DATA  = $_POST;
    
     //obtain parameters from new rates
    $pClass = new paloSantoRecordingGroups($pDB);
     
    $action = getAction();
    
    //into to create new rate
    if($action=="save_new"){
    	$user = getParameter("user");
    	$group= getParameter("recording_group");
		//$arrTrunks = $pBillingRates->getTrunks($pDB2);
		$arrFormNew = createFormNew($arrLang, $arrTrunks);
		$oForm = new paloForm($smarty,$arrFormNew);
		
		if(!$oForm->validateForm($_POST)) {
			$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
			$arrErrores = $oForm->arrErroresValidacion;
			if(is_array($arrErrores) && count($arrErrores) > 0){
				foreach($arrErrores as $k=>$v) {
					$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
				}
			}
			$smarty->assign("mb_message", $strErrorMsg);
			$content=reportAssignGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
			return $content;
		}else{
			$result = $pClass->addToRecordingGroup($user,$group);
			if($result == True){
				$smarty->assign("mb_message", $arrLang["assign_group"]);
				$content = reportRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
				return $content;
			}
		}
    }
    
    if($action=="save_import"){
    	$new_group = getParameter("new_group");
    	 
    	//$arrTrunks = $pBillingRates->getTrunks($pDB2);
    	$arrFormNew = createFormNew($arrLang, $arrTrunks);
    	$oForm = new paloForm($smarty,$arrFormNew);
    
    	if(!$oForm->validateForm($_POST)) {
    		$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
    		$arrErrores = $oForm->arrErroresValidacion;
    		if(is_array($arrErrores) && count($arrErrores) > 0){
    			foreach($arrErrores as $k=>$v) {
    				$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
    			}
    		}
    		$smarty->assign("mb_message", $strErrorMsg);
    		$content=reportAssignGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
    		return $content;
    	}else{
    		$result = $pClass->newRecordingGroup($new_group);
    		if($result == True){
    			$smarty->assign("mb_message", $arrLang["new_group"]);
    			$content = reportRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
    			return $content;
    		}
    	}
    }
    //into to delete rate
    if($action=="delete"){
    	 $id = getParameter("id");
    	 $result = $pClass->deleteGroup($id);
         if($result == true){
             $smarty->assign("mb_message", $arrLang["deleted"]);
             $content = reportRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
             return $content;
         }
         else{
             $smarty->assign("mb_message", $arrLang["deleted_error"]);
             $content = reportViewRecordingGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
             return $content;
         }
     }

     if($action=="save_edit"){
     	$id = getParameter("id");
     	$ext = getParameter("recording_group");

     	$arrFormEdit = createEditForm($arrLang, $arrTrunks);
     	$oForm = new paloForm($smarty,$arrFormEdit);
     
     	if(!$oForm->validateForm($_POST)) {
     		$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
     		$arrErrores = $oForm->arrErroresValidacion;
     		if(is_array($arrErrores) && count($arrErrores) > 0){
     			foreach($arrErrores as $k=>$v) {
     				$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
     			}
     		}
     		//into to edit rate but the field are empty
     		$smarty->assign("mb_message", $strErrorMsg);
     		$content = reportRecordingGroupEdit($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     		return $content;
     	}else{
     		$result = $pClass->addExtension($id,$ext);
     		if($result == True){
     				$smarty->assign("mb_message", $arrLang["edit_group"]);
     				$content = reportRecordingGroupEdit($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     				return $content;
     		}
     	}
     	
     }
     
     if($action=="save_extension"){
     	$id = getParameter("id");
     	$ext = getParameter("add_extension");
     
     	$arrFormEdit = createEditForm($arrLang, $arrTrunks);
     	$oForm = new paloForm($smarty,$arrFormEdit);
     	 
     	if(!$oForm->validateForm($_POST)) {
     		$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
     		$arrErrores = $oForm->arrErroresValidacion;
     		if(is_array($arrErrores) && count($arrErrores) > 0){
     			foreach($arrErrores as $k=>$v) {
     				$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
     			}
     		}
     		//into to edit rate but the field are empty
     		$smarty->assign("mb_message", $strErrorMsg);
     		$content = reportAddExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     		return $content;
     	}else{
     		$result = $pClass->addExtension($id,$ext);
     		if($result == True){
     			$smarty->assign("mb_message", $arrLang["add_extension"]);
     			$content = reportAddExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     			return $content;
     		}
     	}
     
     }
     if($action=="submit_delete_extension"){
     	$id = getParameter("id");
     	$ext = getParameter("del_extension");
     	 
     	$arrFormEdit = createDeleteGroup($arrLang, $arrTrunks);
     	$oForm = new paloForm($smarty,$arrFormEdit);
     	 
     	if(!$oForm->validateForm($_POST)) {
     		$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
     		$arrErrores = $oForm->arrErroresValidacion;
     		if(is_array($arrErrores) && count($arrErrores) > 0){
     			foreach($arrErrores as $k=>$v) {
     				$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
     			}
     		}
     		//into to edit rate but the field are empty
     		$smarty->assign("mb_message", $strErrorMsg);
     		$content = reportDelExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     		return $content;
     	}else{
     		$result = $pClass->delExtension($id,$ext);
     		if($result == True){
     			$smarty->assign("mb_message", $arrLang["remove_extension"]);
     			$content = reportDelExtension($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     			return $content;
     		}
     	}
     	 
     }
     if($action=="submit_delete_from_group"){
     	$id = getParameter("id");
     	$usr = getParameter("usr");
     	 
     	$arrFormEdit = createDeleteGroup($arrLang, $arrTrunks);
     	$oForm = new paloForm($smarty,$arrFormEdit);
     	 
     	if(!$oForm->validateForm($_POST)) {
     		$strErrorMsg = "<b>"._tr('The following fields contain errors').":</b><br/>";
     		$arrErrores = $oForm->arrErroresValidacion;
     		if(is_array($arrErrores) && count($arrErrores) > 0){
     			foreach($arrErrores as $k=>$v) {
     				$strErrorMsg .= "$k: [$v[mensaje]] <br /> ";
     			}
     		}
     		//into to edit rate but the field are empty
     		$smarty->assign("mb_message", $strErrorMsg);
     		$content = reportDeleteFromGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     		return $content;
     	}else{
     		$result = $pClass->delUserFromGroup($id,$usr);
     		if($result == True){
     			$smarty->assign("mb_message", $arrLang["add_extension"]);
     			$content = reportDeleteFromGroup($smarty, $module_name, $local_templates_dir, $pDB, $pDB2, $pDB3, $arrConf, $arrLang);
     			return $content;
     		}
     	}
     	 
     }
}

/**
 * Formulario Asignar Grupo
 * @param unknown $arrLang
 * @param unknown $arrTrunks
 */
function createFormNew($arrUsers, $arrGroups){

  	$arrFields = array(
            "user"   => array(      "LABEL"                  => "User",
                                            "REQUIRED"               => "no",
                                            "INPUT_TYPE"             => "SELECT",
                                            "INPUT_EXTRA_PARAM"      => $arrUsers,
                                            "VALIDATION_TYPE"        => "text",
                                            "VALIDATION_EXTRA_PARAM" => "",
                                            "EDITABLE"               => "si",
                                            ),
            "recording_group"   => array(      "LABEL"                  => "Grupo de Escuchas",
                                            "REQUIRED"               => "no",
                                            "INPUT_TYPE"             => "SELECT",
                                            "INPUT_EXTRA_PARAM"      => $arrGroups,
                                            "VALIDATION_TYPE"        => "text",
                                            "VALIDATION_EXTRA_PARAM" => "",
                                            ),
            );
    return $arrFields;
}

/**
 * Formulario Remover Usuario del Grupo
 * @param unknown $arrLang
 * @param unknown $arrTrunks
 */
function createDeleteGroup($arrUsr){

	$arrFields = array(
			"usr"   => array(      "LABEL"                  => "Users",
					"REQUIRED"               => "no",
					"INPUT_TYPE"             => "SELECT",
					"INPUT_EXTRA_PARAM"      => $arrUsr,
					"VALIDATION_TYPE"        => "text",
					"VALIDATION_EXTRA_PARAM" => "",
			),
	);
	return $arrFields;
}

/**
 * Formulario Nuevo Grupo
 * @param unknown $arrLang
 * @return multitype:multitype:string unknown
 */
function createNewGroupForm($arrLang){
	$arrFields = array(
					"new_group"    => array("LABEL"                  =>	$arrLang["New_group"],
													"REQUIRED"               => "yes",
													"INPUT_TYPE"             => "TEXT",
													"INPUT_EXTRA_PARAM"      => "",
													"VALIDATION_TYPE"        => "text",
													"VALIDATION_EXTRA_PARAM" => ""),
	);
   return $arrFields;
}

function createEditForm($arrUsers, $arrGroups){
	
	$arrFields = array(
			"recording_group"   => array(      "LABEL"                  => "Extensiones",
					"REQUIRED"               => "yes",
					"INPUT_TYPE"             => "SELECT",
					"INPUT_EXTRA_PARAM"      => $arrGroups,
					"VALIDATION_TYPE"        => "text",
					"VALIDATION_EXTRA_PARAM" => "",
			),
	);
	return $arrFields;
}

function createAddExtension($arrGroups){

	$arrFields = array(
			"add_extension"   => array(      "LABEL"                  => "Extensiones",
					"REQUIRED"               => "yes",
					"INPUT_TYPE"             => "SELECT",
					"INPUT_EXTRA_PARAM"      => $arrGroups,
					"VALIDATION_TYPE"        => "text",
					"VALIDATION_EXTRA_PARAM" => "",
			),
	);
	return $arrFields;
}

function createDelExtension($arrGroups){

	$arrFields = array(
			"del_extension"   => array(      "LABEL"                  => "Extensiones",
					"REQUIRED"               => "yes",
					"INPUT_TYPE"             => "SELECT",
					"INPUT_EXTRA_PARAM"      => $arrGroups,
					"VALIDATION_TYPE"        => "text",
					"VALIDATION_EXTRA_PARAM" => "",
			),
	);
	return $arrFields;
}

function getAction()
{
	
    if(getParameter("submit_apply_changes"))
        return "save_edit";
    else if(getParameter("edit"))
        return "edit_form";
    else if(getParameter("addextension"))
    	return "add_extension_form";
    else if(getParameter("save_extension"))
    	return "save_extension";
    else if(getParameter("delextension"))
    	return "del_extension_form";
    else if(getParameter("submit_delete_extension"))
    	return "submit_delete_extension";
    else if(getParameter("delete")) 
        return "delete";
    else if(getParameter("submit_save_rate")) 
        return "save_new";
    else if(getParameter("cancel")) 
        return "cancel";
    else if(getParameter("action")=="view")
        return "view_form";
    else if(getParameter("submit_import_changes"))
        return "save_import";
	else if(getParameter("new_rate"))
        return "new_rate";
	else if(getParameter("submit_delete_from_group"))
		return "submit_delete_from_group";
	else if(getParameter("deluser"))
	 	return "deluser";
	else if(getParameter("import_rate"))
        return "import_rate";
    else
        return "report"; //cancel
}
?>
